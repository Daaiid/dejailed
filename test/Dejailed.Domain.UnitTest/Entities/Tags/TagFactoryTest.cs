﻿using Dejailed.Domain.Entities.Tags;

namespace Dejailed.Domain.UnitTest.Entities.Tags;

public class TagFactoryTest
{
    protected TagFactory _testee = new(new List<ITag>());

    [TestInitialize]
    public void Setup()
    {
        _testee = new TagFactory(new List<ITag>());
    }

    [TestClass]
    public class CreateTag : TagFactoryTest
    {
        [TestMethod]
        public void WithEraTagType_ShouldCreateNewEraTag()
        {
            const string id = "80s";

            var eraTag = _testee.CreateTag<EraTag>(id);

            eraTag.Should().NotBeNull();
            eraTag.Should().BeOfType<EraTag>();
            eraTag!.Id.Should().Be(id);
        }

        [TestMethod]
        public void WithGenreTagType_ShouldCreateNewGenreTag()
        {
            const string id = "Disco";

            var genreTag = _testee.CreateTag<GenreTag>(id);

            genreTag.Should().NotBeNull();
            genreTag.Should().BeOfType<GenreTag>();
            genreTag!.Id.Should().Be(id);
        }

        [TestMethod]
        public void WithMoodTagType_ShouldCreateNewMoodTag()
        {
            const string id = "Chill";

            var moodTag = _testee.CreateTag<MoodTag>(id);

            moodTag.Should().NotBeNull();
            moodTag.Should().BeOfType<MoodTag>();
            moodTag!.Id.Should().Be(id);
        }

        [TestMethod]
        public void WithLanguageTagType_ShouldCreateNewLanguageTag()
        {
            const string id = "German";

            var langTag = _testee.CreateTag<LanguageTag>(id);

            langTag.Should().NotBeNull();
            langTag.Should().BeOfType<LanguageTag>();
            langTag!.Id.Should().Be(id);
        }

        [TestMethod]
        public void TwiceWithIdenticalId_ShouldReturnNull()
        {
            const string id = "someId";

            var firstTag = _testee.CreateTag<EraTag>(id);
            firstTag.Should().NotBeNull();

            var secondTag = _testee.CreateTag<EraTag>(id);
            secondTag.Should().BeNull();
        }

        [TestMethod]
        public void TwiceWithIdenticalIdAndDifferentTypes_ShouldReturnNull()
        {
            const string id = "someId";

            var firstTag = _testee.CreateTag<EraTag>(id);
            firstTag.Should().NotBeNull();

            var secondTag = _testee.CreateTag<MoodTag>(id);
            secondTag.Should().BeNull();
        }
    }

    [TestClass]
    public class GetTag : TagFactoryTest
    {
        [TestMethod]
        public void WithEmptyFactory_ShouldReturnNull()
        {
            var tag = _testee.GetTag("someId");

            tag.Should().BeNull();
        }

        [TestMethod]
        public void WithTagInFactory_ShouldReturnIdenticalTag()
        {
            const string id = "someId";

            var expected = _testee.CreateTag<MoodTag>(id);
            var actual = _testee.GetTag(id);

            actual.Should().BeSameAs(expected);
        }
    }

    [TestClass]
    public class GetAll : TagFactoryTest
    {
        [TestMethod]
        public void WithEmptyFactory_ShouldReturnEmptySet()
        {
            var set = _testee.GetAll();

            set.Should().BeEmpty();
        }

        [TestMethod]
        public void WithTagsInFactory_ShouldReturnAllTags()
        {
            var someTagIds = new string[]{"Calm", "Groovy", "Sleepy"};

            foreach (var id in someTagIds)
            {
                var tag = _testee.CreateTag<MoodTag>(id);
                tag.Should().NotBeNull();
            }

            var tags = _testee.GetAll();

            tags.Should().HaveCount(someTagIds.Length);
        }

    }

}
