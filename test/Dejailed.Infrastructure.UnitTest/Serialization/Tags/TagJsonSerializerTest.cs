using Dejailed.Domain.Entities.Tags;
using Dejailed.Infrastructure.Serlialization.Tags;
using NSubstitute;
using System.Text;
using System.Text.Json;

namespace Dejailed.Infrastructure.UnitTest.Serialization.Tags;

public class TagJsonSerializerTest
{
    private const string EmptyJson = "{}";

    protected ITagSerializer _testee = new TagJsonSerializer();

    [TestInitialize]
    public void Setup()
    {
        _testee = new TagJsonSerializer();
    }

    [TestClass]
    public class Serialize : TagJsonSerializerTest
    {
        private async Task<string> SerializeToString(IReadOnlyCollection<ITag> tags)
        {
            using var memoryStream = new MemoryStream(128);
            await _testee.Serialize(tags, memoryStream);
            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        [TestMethod]
        public async Task WithEmptyList_ShouldCreateEmptyJSONWithVersion()
        {
            const string json = """
                {"FormatVersion":1}
                """;

            var noTags = new List<ITag>();

            var result = await SerializeToString(noTags);

            result.Should().Be(json);
        }

        [TestMethod]
        public async Task WithSingleGenreTag_ShouldCreateJSONWithGenreInArray()
        {
            const string json = """
                {"FormatVersion":1,"Genres":[{"Id":"Rock"}]}
                """;

            var tags = new List<ITag>
            {
                new GenreTag("Rock")
            };

            var result = await SerializeToString(tags);

            result.Should().Be(json);
        }

        [TestMethod]
        public async Task WithNestedGenreTag_ShouldCreateValidJSON()
        {
            const string json = """
                {"FormatVersion":1,"Genres":[{"Id":"Rock","SubTags":[{"Id":"Hard Rock"}]}]}
                """;

            var rockTag = new GenreTag("Rock");
            rockTag.Subs.Add(new GenreTag("Hard Rock"));

            var result = await SerializeToString(new List<ITag>{ rockTag });

            result.Should().Be(json);
        }

        [TestMethod]
        public async Task WithOnlyMoodTag_ShouldCreateValidJSON()
        {
            const string json = """
                {"FormatVersion":1,"Moods":[{"Id":"Chill"}]}
                """;

            var tags = new List<ITag>
            {
                new MoodTag("Chill")
            };

            var result = await SerializeToString(tags);

            result.Should().Be(json);
        }

        [TestMethod]
        public async Task WithOnlyEraTag_ShouldCreateValidJSON()
        {
            const string json = """
                {"FormatVersion":1,"Eras":[{"Id":"90s"}]}
                """;

            var tags = new List<ITag>
            {
                new EraTag("90s")
            };

            var result = await SerializeToString(tags);

            result.Should().Be(json);
        }
        
        [TestMethod]
        public async Task WithOnlyLanguageTag_ShouldCreateValidJSON()
        {
            const string json = """
                {"FormatVersion":1,"Languages":[{"Id":"German"}]}
                """;

            var tags = new List<ITag>
            {
                new LanguageTag("German")
            };

            var result = await SerializeToString(tags);

            result.Should().Be(json);
        }

        [TestMethod]
        public async Task WithAllTags_ShouldCreateValidJSON()
        {
            const string json = """
                {"FormatVersion":1,"Genres":[{"Id":"lofi"}],"Moods":[{"Id":"Dancing"}],"Languages":[{"Id":"French"}],"Eras":[{"Id":"Baroque"}]}
                """;

            var tags = new List<ITag>
            {
                new GenreTag("lofi"),
                new MoodTag("Dancing"),
                new LanguageTag("French"),
                new EraTag("Baroque")
            };

            var result = await SerializeToString(tags);

            result.Should().Be(json);
        }

        [TestMethod]
        public async Task WithMultipleGenreTags_ShouldCreateValidJSON()
        {
            const string json = """
                {"FormatVersion":1,"Genres":[{"Id":"lofi"},{"Id":"HipHop"},{"Id":"Rap"}]}
                """;

            var tags = new List<ITag>
            {
                new GenreTag("lofi"),
                new GenreTag("HipHop"),
                new GenreTag("Rap")
            };

            var result = await SerializeToString(tags);

            result.Should().Be(json);
        }

        [TestMethod]
        public async Task ShouldNotCloseStream()
        {
            var stream = Substitute.For<Stream>();
            
            var tags = new List<ITag>
            {
                new GenreTag("World")
            };

            await _testee.Serialize(tags, stream);

            stream.DidNotReceive().Close();
        }
    }

    [TestClass]
    public class Deserialize : TagJsonSerializerTest
    {
        private static Stream CreateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value));
        }

        [TestMethod]
        public async Task WithEmptyObject_ShouldCreateEmptyList()
        {
            using var stream = CreateStreamFromString(EmptyJson);

            var result = await _testee.Deserialize(stream);

            result.Should().BeEmpty();
        }

        [TestMethod]
        public void WithIncompatibleFormatVersion_ShouldThrow()
        {
            const string json = """
                {"FormatVersion":69}
                """;

            using var stream = CreateStreamFromString(json);

            var action = () => _testee.Deserialize(stream).Result;

            action.Should().Throw<FormatException>();
        }

        [TestMethod]
        public void WithEmptyInputString_ShouldThrow()
        {
            using var stream = CreateStreamFromString(string.Empty);

            var action = () => _testee.Deserialize(stream).Result;

            action.Should().Throw<JsonException>();
        }

        // TODO (Said, 2022-12-2): More test cases covering normal input (similar to serialize)

        [TestMethod]
        public async Task ShouldNotCloseStream()
        {
            var stream = Substitute.ForPartsOf<MemoryStream>(Encoding.UTF8.GetBytes(EmptyJson));                
            
            _ = await _testee.Deserialize(stream);

            stream.DidNotReceive().Close();
        }
    }
}
