using Dejailed.Infrastructure.Auth;
using SpotifyAPI.Web;

namespace Dejailed.Infrastructure.IntegrationTest.Auth;

[TestClass]
public static class SpotifyAuthenticatorTest
{
    private const int _callbackPort = 5000;
    private static readonly Uri _callbackUri = new($"http://localhost:{_callbackPort}/callback");

    public static SpotifyClient Client { get; private set; } = default!;

    [AssemblyInitialize]
    public static async Task LoginManually(TestContext _)
    {
        var auth = new SpotifyAuthenticator(_callbackUri, _callbackPort);

        using var cts = new CancellationTokenSource();
        cts.CancelAfter(TimeSpan.FromMinutes(2));

        var client = await auth.Login(Secrets.MyCredentials, AuthConfig.RequiredAppScopes, cts.Token);

        client.Should().NotBeNull();

        Client = client;
    }
}
