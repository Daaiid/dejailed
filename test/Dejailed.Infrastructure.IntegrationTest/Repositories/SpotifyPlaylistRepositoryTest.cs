﻿using Dejailed.Infrastructure.IntegrationTest.Auth;
using Dejailed.Infrastructure.Spotify;

namespace Dejailed.Infrastructure.IntegrationTest.Repositories;

[TestClass]
public class SpotifyPlaylistRepositoryTest
{
    private readonly SpotifyPlaylistRepository _repo;

    public SpotifyPlaylistRepositoryTest()
    {
        _repo = new SpotifyPlaylistRepository(SpotifyAuthenticatorTest.Client);
    }

    [TestMethod]
    public async Task GetAllForCurrentUser_ShouldReturnPlaylistsWithoutSongs()
    {
        var playlists = await _repo.GetAll();
        
        playlists.Should().NotBeNullOrEmpty();
        playlists.Should().OnlyContain(pl => !pl.AreTracksLoaded, because: "eager-loading all tracks would be slow.");
    }

}
