# Dejailed

Trying to make managing playlists on Spotify a bit less of a pain. This project is a WIP.

## Compiling

Compiling the solution requires the .NET 7 SDK to be installed.

## Talking to the Spotify API

In order to compile and run integration tests, you need to register a new app under Spotify Developer (other authentication methods aren't supported yet).
Under "src/Dejailed.Infrastructure/Auth" add a "Secrets.cs"" file with the following structure and your own secrets. DO NOT CHECK IN THIS FILE:

```cs
namespace Dejailed.Infrastructure.Auth;

internal static class Secrets
{
    public static readonly ClientCredentials MyCredentials = 
        new(ClientId: "*clientId*", ClientSecret: "*clientSecret*");
}
```
