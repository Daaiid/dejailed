# DeJailed

## Goals
* Complex Playlists
	* Albums
	* Artists
	* Nested Playlists
	* Songs
* Easily answer the question "Have I categorized this song already?"
* Make it all flow very well (good UX)
* Learn to consume a 3rd Party API (well I was able to cheat a little there :3)
* Improve Unit Testing skills

## Optional Features (Head in the Clouds)
* Make a DSL (domain specific language) similar to SQL to declaritively build playlists

## TODO
* Authorize with developer credentials (DONE)
* Clean up the mess (DONE)
* Deserialize AuthToken (DONE)
* Use token and docs to learn about out the API (DONE)
* Start building up the domain model
	* Make separate types from the given data types in the API Library
* Start using Git at some point
* Build towards a through line (Data from Web API -> UI)
* Start to build out the first use cases
