﻿using Dejailed.Application.Repositories;
using Dejailed.Domain.Entities;
using SpotifyAPI.Web;

namespace Dejailed.Infrastructure.Spotify;

internal sealed class SpotifyPlaylistRepository : IPlaylistRepository
{
    private const int MaximumPerRequestLimit = 50;

    private static readonly PlaylistCurrentUsersRequest _currentUserRequest = new()
    {
        Limit = MaximumPerRequestLimit
    };

    private static readonly PlaylistGetItemsRequest _getItemsRequest = new()
    {
        Limit = MaximumPerRequestLimit
    };

    private readonly SpotifyClient _client;

    public SpotifyPlaylistRepository(SpotifyClient client)
    {
        _client = client;
    }

    public async Task<List<Playlist>> GetAll()
    {
        var pagedPlaylists = await _client.Playlists.CurrentUsers(_currentUserRequest);

        if (pagedPlaylists.Total > MaximumPerRequestLimit)
        {
            return await GetRemainingPlaylists(pagedPlaylists);
        }

        return pagedPlaylists.Items?
            .Select(MappingExtensions.ToDomainObject)
            .ToList()
            ?? new List<Playlist>();
    }

    private async Task<List<Playlist>> GetRemainingPlaylists(Paging<SimplePlaylist> firstPage)
    {
        var playlists = new List<Playlist>(MaximumPerRequestLimit * 2);

        await foreach (var playlist in _client.Paginate(firstPage))
        {
            playlists.Add(playlist.ToDomainObject());
        }

        return playlists;
    }

    public async Task<Playlist> GetAllSongs(Playlist playlist)
    {
        if (playlist.AreTracksLoaded)
        {
            return playlist;
        }

        var pagedSongs = await _client.Playlists.GetItems(playlist.Id, _getItemsRequest);

        if (pagedSongs.Total > MaximumPerRequestLimit)
        {
            var allSongs = await GetRemainingSongs(pagedSongs);
            return FillPlaylist(allSongs, playlist);
        }

        if (pagedSongs.Items is null)
        {
            return playlist;
        }

        var songs = pagedSongs.Items
            .Select(MappingExtensions.ToDomainObject)
            .ToList();

        return FillPlaylist(songs, playlist);

        static Playlist FillPlaylist(IReadOnlyList<Song> songs, Playlist playlist)
        {
            return new Playlist(
                playlist.Id,
                playlist.Title,
                playlist.SnapshotId,
                songs);
        }
    }

    private async Task<List<Song>> GetRemainingSongs(Paging<PlaylistTrack<IPlayableItem>> firstPage)
    {
        var songs = new List<Song>(MaximumPerRequestLimit * 2);

        await foreach (var song in _client.Paginate(firstPage))
        {
            songs.Add(song.ToDomainObject());
        }

        return songs;
    }


}
