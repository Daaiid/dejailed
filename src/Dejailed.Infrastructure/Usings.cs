﻿global using System.Diagnostics;

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Dejailed.Infrastructure.IntegrationTest")]
[assembly: InternalsVisibleTo("Dejailed.Infrastructure.UnitTest")]
