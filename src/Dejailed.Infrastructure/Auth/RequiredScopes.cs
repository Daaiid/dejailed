﻿using SpotifyAPI.Web;
using System.Collections.Immutable;

namespace Dejailed.Infrastructure.Auth;

internal static class AuthConfig
{
    public static readonly ImmutableArray<string> RequiredAppScopes = new string[]
    {
        Scopes.PlaylistModifyPublic,
        Scopes.PlaylistModifyPrivate,
        Scopes.PlaylistReadPrivate,
        Scopes.UserLibraryModify
    }.ToImmutableArray();
    
}
