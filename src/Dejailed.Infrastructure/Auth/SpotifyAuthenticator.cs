﻿using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;

namespace Dejailed.Infrastructure.Auth;

// More nuanced implementation of the example at https://johnnycrazy.github.io/SpotifyAPI-NET/docs/authorization_code
internal sealed class SpotifyAuthenticator
{
    private static readonly TimeSpan ResponseWaitInterval = TimeSpan.FromMilliseconds(100);

    private readonly Uri _callbackUri;
    private readonly int _callbackPort;

    private EmbedIOAuthServer? _server;
    private AuthorizationCodeResponse? _response;

    public SpotifyAuthenticator(Uri callbackUri, int callbackPort)
    {
        _callbackUri = callbackUri;
        _callbackPort = callbackPort;
    }

    public async Task<SpotifyClient> Login(ClientCredentials credentials, ICollection<string> scopes, CancellationToken cancellationToken)
    {
        _server = new EmbedIOAuthServer(_callbackUri, _callbackPort);

        _server.AuthorizationCodeReceived += HandleAuthCodeReceived;
        _server.ErrorReceived += HandleErrorReceived;

        await _server.Start();

        var loginRequest = new LoginRequest(_callbackUri, credentials.ClientId, LoginRequest.ResponseType.Code)
        {
            Scope = scopes
        };

        BrowserUtil.Open(loginRequest.ToUri());

        while (!cancellationToken.IsCancellationRequested && _response is null)
        {
            await Task.Delay(ResponseWaitInterval, cancellationToken);
        }

        cancellationToken.ThrowIfCancellationRequested();
        Debug.Assert(_response is not null);

        var config = SpotifyClientConfig.CreateDefault();
        var tokenResponse = await new OAuthClient(config).RequestToken(
            new AuthorizationCodeTokenRequest(
                credentials.ClientId,
                credentials.ClientSecret,
                _response.Code,
                _callbackUri));

        return new SpotifyClient(tokenResponse);
    }

    private async Task HandleAuthCodeReceived(object sender, AuthorizationCodeResponse response)
    {
        await TeardownServer();

        _response = response;
    }

    private async Task HandleErrorReceived(object sender, string errorMessage, string? state)
    {
        // TODO: Add proper logging for the failure case.
        Debug.WriteLine($"Login failed: {errorMessage}");
        await TeardownServer();
    }

    private async Task TeardownServer()
    {
        if (_server is null)
        {
            return;
        }

        await _server!.Stop();
        _server.AuthorizationCodeReceived -= HandleAuthCodeReceived;
        _server.ErrorReceived -= HandleErrorReceived;
        _server.Dispose();
        _server = null;
    }
}
