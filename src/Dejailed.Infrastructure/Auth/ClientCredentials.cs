﻿namespace Dejailed.Infrastructure.Auth;

internal sealed record ClientCredentials(string ClientId, string ClientSecret);
