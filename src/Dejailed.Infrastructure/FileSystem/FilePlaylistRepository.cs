﻿using Dejailed.Application.Repositories;
using Dejailed.Domain.Entities;
using Dejailed.Infrastructure.Serlialization.Playlists;

namespace Dejailed.Infrastructure.FileSystem;

internal sealed class FilePlaylistRepository : IPlaylistRepository
{
    private readonly IPlaylistSerializer _serializer;
    private readonly FilePathProvider _pathProvider;

    public FilePlaylistRepository(IPlaylistSerializer serializer, FilePathProvider pathProvider)
    {
        _serializer = serializer;
        _pathProvider = pathProvider;
    }

    public async Task<List<Playlist>> GetAll()
    {
        string filePath = _pathProvider.SavedPlaylists;

        if (!File.Exists(filePath))
        {
            return new List<Playlist>();
        }

        using var fileStream = File.OpenRead(filePath);

        return await _serializer.Deserialize(fileStream);
    }

    public Task<Playlist> GetAllSongs(Playlist playlist)
    {
        Debug.Fail("This should only be called on a repository that " +
            "fetches the songs in the playlist remotely.\n" +
            "Check call site that entered here, there might be something wrong.");

        return Task.FromResult(playlist);
    }
}
