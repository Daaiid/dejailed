﻿namespace Dejailed.Infrastructure.FileSystem;

public sealed class FilePathProvider
{
    public string SavedTags { get; }
    public string SavedPlaylists { get; }

    public FilePathProvider(string savedTags, string savedPlaylists)
    {
        SavedTags = savedTags;
        SavedPlaylists = savedPlaylists;
    }

}
