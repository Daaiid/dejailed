﻿using Dejailed.Application.Repositories;
using Dejailed.Domain.Entities.Tags;
using Dejailed.Infrastructure.Serlialization.Tags;

namespace Dejailed.Infrastructure.FileSystem;

public sealed class FileTagRepository : ITagRepository
{
    private readonly ITagSerializer _serializer;
    private readonly FilePathProvider _pathProvider;

    public FileTagRepository(ITagSerializer serializer, FilePathProvider pathProvider)
    {
        _serializer = serializer;
        _pathProvider = pathProvider;
    }

    public async Task<List<ITag>> GetAll()
    {
        string filePath = _pathProvider.SavedTags;

        if (!File.Exists(filePath))
        {
            return new List<ITag>();
        }

        using var fileStream = File.OpenRead(filePath);

        return await _serializer.Deserialize(fileStream);        
    }

    public async Task Save(IReadOnlyCollection<ITag> tags)
    {
        string filePath = _pathProvider.SavedTags;

        using var fileStream = File.OpenWrite(filePath);

        await _serializer.Serialize(tags, fileStream);
    }
}
