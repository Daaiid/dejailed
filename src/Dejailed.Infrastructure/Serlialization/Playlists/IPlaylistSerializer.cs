﻿using Dejailed.Domain.Entities;

namespace Dejailed.Infrastructure.Serlialization.Playlists;

public interface IPlaylistSerializer
{
    Task Serialize(IReadOnlyCollection<Playlist> playlists, Stream outStream);
    Task<List<Playlist>> Deserialize(Stream inStream);
}
