﻿using Dejailed.Domain.Entities.Tags;

namespace Dejailed.Infrastructure.Serlialization.Tags;

public interface ITagSerializer
{
    Task Serialize(IReadOnlyCollection<ITag> tags, Stream outStream);
    Task<List<ITag>> Deserialize(Stream inStream);
}
