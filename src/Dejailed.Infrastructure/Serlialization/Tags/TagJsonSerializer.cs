﻿using Dejailed.Domain.Entities.Tags;
using System.Text.Json;

namespace Dejailed.Infrastructure.Serlialization.Tags;

internal sealed class TagJsonSerializer : ITagSerializer
{
    private static readonly JsonSerializerOptions _options = new()
    {
        DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull
    };

    public async Task Serialize(IReadOnlyCollection<ITag> tags, Stream outStream)
    {
        var saveable = new SaveableTags
        {
            Genres = ToSerializable(tags.OfType<GenreTag>()),
            Moods = ToSerializable(tags.OfType<MoodTag>()),
            Languages = ToSerializable(tags.OfType<LanguageTag>()),
            Eras = ToSerializable(tags.OfType<EraTag>())
        };

        await JsonSerializer.SerializeAsync(outStream, saveable, _options);

        static SerializableTag[]? ToSerializable(IEnumerable<ITag> tags)
        {
            var result = new List<SerializableTag>();

            foreach (var tag in tags)
            {
                var serializable = new SerializableTag
                {
                    Id = tag.Id,
                    SubTags = ToSerializable(tag.SubTags)
                };

                result.Add(serializable);
            }

            if (result.Count == 0)
            {
                return null;
            }

            return result.ToArray();
        }
    }

    public async Task<List<ITag>> Deserialize(Stream stream)
    {
        var loadedTags = await JsonSerializer.DeserializeAsync<SaveableTags>(stream);

        var tags = new List<ITag>();

        if (loadedTags is null)
        {
            return tags;
        }

        if (loadedTags.FormatVersion != SaveableTags.CurrentFormatVersion)
        {
            throw new FormatException("Unsupported file format encountered");
        }

        if (loadedTags.Genres is not null)
        {
            tags.AddRange(FromSerializable(loadedTags.Genres));
        }

        if (loadedTags.Moods is not null)
        {
            foreach (var tag in loadedTags.Moods)
            {
                tags.Add(new MoodTag(tag.Id!));
            }
        }

        if (loadedTags.Eras is not null)
        {
            foreach (var tag in loadedTags.Eras)
            {
                tags.Add(new EraTag(tag.Id!));
            }
        }

        if (loadedTags.Languages is not null)
        {
            foreach (var tag in loadedTags.Languages)
            {
                tags.Add(new LanguageTag(tag.Id!));
            }
        }

        return tags;

        static List<GenreTag> FromSerializable(SerializableTag[] tags)
        {
            var result = new List<GenreTag>();

            foreach (var tag in tags)
            {
                var genreTag = new GenreTag(tag.Id!);
                var subtags = tag.SubTags;

                if (subtags is not null && subtags.Length > 0)
                {
                    genreTag.Subs = FromSerializable(subtags).ToHashSet();
                }

                result.Add(genreTag);
            }

            return result;
        }

    }


    private sealed class SaveableTags
    {
        public const int CurrentFormatVersion = 1;

        public int FormatVersion { get; set; } = CurrentFormatVersion;

        public SerializableTag[]? Genres { get; set; }
        public SerializableTag[]? Moods { get; set; }
        public SerializableTag[]? Languages { get; set; }
        public SerializableTag[]? Eras { get; set; }
    }

    private sealed class SerializableTag
    {
        public string? Id { get; set; }

        public SerializableTag[]? SubTags { get; set; }
    }

}
