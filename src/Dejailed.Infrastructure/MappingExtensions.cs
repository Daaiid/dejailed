﻿using Dejailed.Domain.Entities;
using SpotifyAPI.Web;

namespace Dejailed.Infrastructure;

internal static class MappingExtensions
{
    public static Playlist ToDomainObject(this SimplePlaylist apiObj)
    {
        return new Playlist(
            apiObj.Id,
            apiObj.Name,
            apiObj.SnapshotId,
            Array.Empty<Song>());
    }

    public static Song ToDomainObject(this PlaylistTrack<IPlayableItem> apiObj)
    {
        if (apiObj.Track is FullTrack track)
        {
            return new Song(
                track.Id,
                track.Name,
                track.Artists.Select(ToDomainObject).ToList()
            );
        }

        throw new NotImplementedException("Handling podcast episodes not implemented yet.");
    }

    public static Artist ToDomainObject(this SimpleArtist apiObj)
    {
        return new Artist(apiObj.Id, apiObj.Name);
    }
}
