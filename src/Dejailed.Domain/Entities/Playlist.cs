﻿namespace Dejailed.Domain.Entities;

public sealed record Playlist
{
    public IReadOnlyList<Song> Tracks { get; }
    public string Id { get; }
    public string Title { get; }
    public string SnapshotId { get; }

    public bool AreTracksLoaded => Tracks.Count > 0;
    

    public Playlist(string id, string title, string snapshotId, IReadOnlyList<Song> tracks)
    {
        Tracks = tracks;
        Id = id;
        Title = title;
        SnapshotId = snapshotId;
    }
}
