﻿namespace Dejailed.Domain.Entities;

public sealed record Song(string Id, string Title, IReadOnlyList<Artist> Artists);
