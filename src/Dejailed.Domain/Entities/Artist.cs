﻿namespace Dejailed.Domain.Entities;

public sealed record Artist(string Id, string Name);
