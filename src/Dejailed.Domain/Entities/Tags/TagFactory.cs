﻿using System.Diagnostics;

namespace Dejailed.Domain.Entities.Tags;

public sealed class TagFactory : ITagFactory
{
    private static readonly IReadOnlyDictionary<Type, Func<string, ITag>> _factories =
        new Dictionary<Type, Func<string, ITag>>
        {
            { typeof(EraTag), id => new EraTag(id) },
            { typeof(MoodTag), id => new MoodTag(id) },
            { typeof(GenreTag), id => new GenreTag(id) },
            { typeof(LanguageTag), id => new LanguageTag(id) }
        };

    private readonly IDictionary<string, ITag> _tags;

    public TagFactory(IReadOnlyCollection<ITag> initialTags)
    {
        _tags = initialTags
            .ToDictionary(tag => tag.Id, tag => tag);
    }

    public ITag? GetTag(string id)
    {
        if (_tags.TryGetValue(id, out var tag))
        {
            return tag;
        }

        return null;
    }

    public ITag? CreateTag<T>(string id) where T : ITag
    {
        var factory = _factories[typeof(T)];
        var newTag = factory(id);

        if (_tags.ContainsKey(id))
        {
            return null;
        }

        _tags.Add(id, newTag);
        
        return newTag;
    }

    public IReadOnlySet<ITag> GetAll()
    {
        return _tags.Values.ToHashSet();
    }



#if DEBUG
    /// <summary>
    /// Checks invariants of <see cref="_factories"/>
    /// </summary>
    static TagFactory()
    {
        foreach (var factoryDef in _factories)
        {
            var key = factoryDef.Key;
            Debug.Assert(key.IsAssignableTo(typeof(ITag)), 
                $"Key is not a derived type of ITag. Faulty type: {key.GetType()}");

            var val = factoryDef.Value;
            var result = val("someId");
            Debug.Assert(key == result.GetType(),
                "The created instance is not the same type as the key. " +
                $"Key type: {key}; instance type: {result.GetType()}");
        }
    }
#endif

}
