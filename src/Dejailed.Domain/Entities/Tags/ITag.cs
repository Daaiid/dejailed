﻿namespace Dejailed.Domain.Entities.Tags;

public interface ITag : IEquatable<ITag>
{
    /// <summary>
    /// Unique name of the tag.
    /// </summary>
    string Id { get; }

    /// <summary>
    /// If this tag has more specific subtags we can get them here.
    /// Otherwise will return empty set.
    /// </summary>
    IReadOnlySet<ITag> SubTags { get; }
}
