﻿namespace Dejailed.Domain.Entities.Tags;

public sealed class EraTag : TagBase
{
    internal EraTag(string id)
        : base(id)
    {
    }
}
