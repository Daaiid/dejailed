﻿namespace Dejailed.Domain.Entities.Tags;

public sealed class MoodTag : TagBase
{
    internal MoodTag(string id)
        : base(id)
    {
    }
}
