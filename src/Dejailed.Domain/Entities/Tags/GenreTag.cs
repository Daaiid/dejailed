﻿namespace Dejailed.Domain.Entities.Tags;

public sealed class GenreTag : TagBase
{
    public override IReadOnlySet<ITag> SubTags => new HashSet<ITag>(Subs);

    public IReadOnlySet<GenreTag> SubGenres => Subs;
    public IReadOnlySet<GenreTag> ParentGenres => Parents;

    internal HashSet<GenreTag> Subs { get; set; } = new HashSet<GenreTag>();
    internal HashSet<GenreTag> Parents { get; } = new HashSet<GenreTag>();

    internal GenreTag(string id)
        : base(id)
    {
    }
}
