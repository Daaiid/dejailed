﻿namespace Dejailed.Domain.Entities.Tags;

public abstract class TagBase : ITag
{
    private static readonly IReadOnlySet<ITag> EmptySet = new HashSet<ITag>();

    public string Id { get; }

    public virtual IReadOnlySet<ITag> SubTags => EmptySet;

    protected internal TagBase(string id)
    {
        Id = id;
    }

    public override bool Equals(object? other)
    {
        if (other is ITag tag)
        {
            return Equals(tag);
        }

        return base.Equals(other);
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }

    public bool Equals(ITag? other)
    {
        if (other is null)
        {
            return false;
        }

        if (other.GetType() != GetType())
        {
            return false;
        }

        return other.Id == Id;
    }
}
