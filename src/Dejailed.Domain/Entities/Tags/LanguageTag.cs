﻿namespace Dejailed.Domain.Entities.Tags;

public sealed class LanguageTag : TagBase
{
    internal LanguageTag(string id)
        : base(id)
    {
    }
}
