﻿namespace Dejailed.Domain.Entities.Tags;

public interface ITagFactory
{
    /// <summary>
    /// Returns a tag with a matching id, otherwise null.
    /// </summary>
    ITag? GetTag(string id);

    /// <summary>
    /// Creates a new tag.
    /// </summary>
    /// <typeparam name="T">The underlying type the tag should have.</typeparam>
    /// <returns>The created tag or null if id already taken.</returns>
    ITag? CreateTag<T>(string id) where T : ITag;

    /// <summary>
    /// Returns all tags currently available
    /// </summary>
    IReadOnlySet<ITag> GetAll();
}
