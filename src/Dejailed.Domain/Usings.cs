﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Dejailed.Infrastructure")]
[assembly: InternalsVisibleTo("Dejailed.Infrastructure.UnitTest")]
