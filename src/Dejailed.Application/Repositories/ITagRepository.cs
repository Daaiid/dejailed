﻿using Dejailed.Domain.Entities.Tags;

namespace Dejailed.Application.Repositories;

public interface ITagRepository
{
    Task<List<ITag>> GetAll();

    Task Save(IReadOnlyCollection<ITag> tags);

}
