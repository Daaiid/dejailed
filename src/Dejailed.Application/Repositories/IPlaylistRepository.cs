﻿using Dejailed.Domain.Entities;

namespace Dejailed.Application.Repositories;

public interface IPlaylistRepository
{
    /// <summary>
    /// Returns the all playlists in the repository. 
    /// This might return empty playlists (no songs assigned) or an empty list.
    /// </summary>
    Task<List<Playlist>> GetAll();

    /// <summary>
    /// Fills the presumably empty playlist, gets the songs
    /// that are associated with it and places them on a new playlist object.
    /// Returns the same object if the playlist is not empty.
    /// </summary>
    Task<Playlist> GetAllSongs(Playlist playlist);
}
